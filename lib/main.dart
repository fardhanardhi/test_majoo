import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_majoo/data/blocs/bottom_nav_cubit/bottom_nav_cubit.dart';
import 'package:test_majoo/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_majoo/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_majoo/util/theme.dart';
import 'package:test_majoo/util/transitions.dart';

import 'ui/screens/screens.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late AuthCubit authCubit;
  late BottomNavCubit bottomNavCubit;
  late PeopleCubit peopleCubit;
  late FavPeopleCubit favPeopleCubit;

  @override
  void initState() {
    super.initState();
    authCubit = AuthCubit()..appStarted();
    bottomNavCubit = BottomNavCubit();
    peopleCubit = PeopleCubit();
    favPeopleCubit = FavPeopleCubit();
  }

  @override
  void dispose() {
    authCubit.close();
    bottomNavCubit.close();
    peopleCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => authCubit),
        BlocProvider(create: (_) => bottomNavCubit),
        BlocProvider(create: (_) => peopleCubit),
        BlocProvider(create: (_) => favPeopleCubit),
      ],
      child: MaterialApp(
        title: 'Mono',
        debugShowCheckedModeBanner: false,
        theme: AppTheme(isDark: false).themeData,
        builder: (context, child) => Stack(
          children: [
            Container(
              color: Colors.black,
            ),
            Positioned.fill(
              child: ClipRRect(
                borderRadius: const BorderRadius.vertical(
                  bottom: Radius.circular(30),
                ),
                child: Material(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: child,
                ),
              ),
            )
          ],
        ),
        home: Scaffold(
          body: BlocBuilder<AuthCubit, AuthState>(
            bloc: authCubit,
            builder: (context, state) {
              return ASharedAxisTransitionSwitcher(
                transitionType: SharedAxisTransitionType.scaled,
                duration: const Duration(milliseconds: 300),
                reverse: state is AuthUnauthenticated,
                child: state is AuthInitial
                    ? const SplashScreen()
                    : state is AuthAuthenticated
                        ? const MainScreen()
                        : state is AuthUnauthenticated
                            ? const LoginScreen()
                            : const SplashScreen(),
              );
            },
          ),
        ),
      ),
    );
  }
}
