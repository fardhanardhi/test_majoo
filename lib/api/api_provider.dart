import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:test_majoo/api/api.dart';

class ApiProvider {
  Future<dynamic> get(dynamic url, {Map<String, String>? headers}) async {
    dynamic responseJson;
    try {
      final response = await http.get(Uri.parse(url), headers: headers);

      responseJson = _returnResponse(response);
    } on SocketException {
      throw NetworkException('Tidak ada koneksi internet');
    }
    return responseJson;
  }

  Future<dynamic> post(String url,
      {dynamic body, Map<String, String>? headers}) async {
    dynamic responseJson;
    try {
      final response =
          await http.post(Uri.parse(url), body: body, headers: headers);

      responseJson = _returnResponse(response);
    } on SocketException {
      throw NetworkException('Tidak ada koneksi internet');
    }
    return responseJson;
  }

  Future<dynamic> put(String url,
      {dynamic body, Map<String, String>? headers}) async {
    dynamic responseJson;
    try {
      final response =
          await http.put(Uri.parse(url), body: body, headers: headers);

      responseJson = _returnResponse(response);
    } on SocketException {
      throw NetworkException('Tidak ada koneksi internet');
    }
    return responseJson;
  }

  Future<dynamic> delete(String url, {Map<String, String>? headers}) async {
    dynamic responseJson;
    try {
      final response = await http.delete(Uri.parse(url), headers: headers);

      responseJson = _returnResponse(response);
    } on SocketException {
      throw NetworkException('Tidak ada koneksi internet');
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    dynamic responseJson = json.decode(response.body.toString());
    final error = responseJson['message'] ?? 'Terjadi kesalahan';

    if (kDebugMode) {
      String responseJsonStr = response.body;
      String? endpointStr = response.request?.url.toString();
      String? endpointMethod = response.request?.method;

      debugPrint('\x1B[31m\n->\x1B[0m');
      debugPrint('\x1B[37m[$endpointMethod] $endpointStr\x1B[0m');
      debugPrint('\x1B[33m$responseJsonStr\x1B[0m');
    }

    switch (response.statusCode) {
      case 200:
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw error;
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw ServerException(error);
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
