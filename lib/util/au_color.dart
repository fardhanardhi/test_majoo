import 'dart:ui';
import 'package:flutter/material.dart';

class AUColor {
  const AUColor._();

  /// main color scheme
  static const primary = Color(0xFF121212);
  static const primaryVariant = Color(0xFF121212);
  static const secondary = Color(0xFF121212);
  static const secondaryVariant = Color(0xFF121212);
  static const background = Colors.white;
  static const surface = Colors.white;
  static const onBackground = Colors.black;
  static const onSurface = Colors.black;
  static const onError = Colors.white;
  static const onPrimary = Colors.white;
  static const onSecondary = Colors.white;
  static const error = Color(0xFFE74036);
  static const warning = Color(0xFFFEC107);

  //shadow
  static const primaryShadow = Color(0xFF838383);

  /// screen text color
  static const primaryText = Colors.black;
  static const secondaryText = Color(0xFF777C7E);
  static const thirdText = Color(0xFF9A9A9A);
  static const fourthText = Color(0xFF959595);
  static const accentText = primary;

  /// [HvTextField]
  static const textFieldFill = Color(0xFFF4F4F4);
  static const textFieldHint = Color(0xFF8F9497);

  /// [HvSearchField]
  static const searchFieldBorder = Color(0xFFE3E3E3);

  /// [HvButton]
  static const buttonDisabledBackground = Color(0xFFF4F4F4);
  static const buttonDisabledForeground = Color(0xFF8F9497);

  static const customAppBarBackground = Color(0xFFE7E4E4);

  ///
  static const dividerContent = Color(0xFFEAEAEA);
  static const dividerGeneral = Color(0xFFD4D4D4);
  static const infoColor = Color(0xFF3DB6D9);
  static const infoCard = Color(0xFFD0E7ED);
}
