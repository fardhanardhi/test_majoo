class AValidator {
  final bool validateLength;

  AValidator([this.validateLength = false]);
  String? validateRequired(String? value) {
    if (value?.isEmpty ?? false) {
      return "Required!";
    }
    return null;
  }

  String? validateName(String? value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Name is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  String? validateUsername(String? value) {
    String pattern = r'(^(?=[a-z0-9]*$))';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Username is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Username must be a-z and 0-9";
    }
    return null;
  }

  String? validateMobile(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Mobile phone number is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Mobile phone number must contain only digits";
    }
    return null;
  }

  String? validateNumber(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Number is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Number must contain only digits";
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value?.isEmpty ?? false) {
      return "Password is required";
    } else if ((value != null && value.length < 6) && validateLength) {
      return 'Password must be more than 5 charaters';
    } else {
      return null;
    }
  }

  String? validateEmail(String? value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern.toString());
    if (value != null && !regex.hasMatch(value)) {
      return 'Enter Valid Email';
    } else {
      return null;
    }
  }

  String? validateConfirmPassword(String password, String confirmPassword) {
    if (password != confirmPassword) {
      return 'Password doesn\'t match';
    } else if (confirmPassword.isEmpty) {
      return 'Confirm password is required';
    } else {
      return null;
    }
  }

  //==============Form catch entry page==============

  String? validateNameFish(String? value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Fish name is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  String? validateDateCatch(String? value) {
    // String pattern = r'(^[a-zA-Z ]*$)';
    // RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "date catch is required";
    }
    return null;
  }

  String? validateTotalCatch(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Total catch is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Total catch must contain only digits";
    }
    return null;
  }

  String? validateWeight(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Weight is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "weight must contain only digits";
    }
    return null;
  }

  String? validateFloatingWeight(String? value) {
    String pattern = r'(^[1-9]\d*(\.\d+)?$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Weight is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "weight must contain only digits";
    }
    return null;
  }

  String? validateSoldPrice(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Sold price is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Sold price must contain only digits";
    }
    return null;
  }

  //==============Form Activation account user==============
  String? validateNameUser(String? value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Name user is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

  String? validateAddress(String? value) {
    if (value?.isEmpty ?? false) {
      return "Address is required";
    }
    return null;
  }

  String? validateProvince(String? value) {
    if (value?.isEmpty ?? false) {
      return "Province is required";
    }
    return null;
  }

  String? validateCity(String? value) {
    if (value?.isEmpty ?? false) {
      return "City is required";
    }
    return null;
  }

  String? validateDistrict(String? value) {
    if (value?.isEmpty ?? false) {
      return "District is required";
    }
    return null;
  }

  //==============Form Activation KTP==============
  String? validateNumberKtp(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "No KTP is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "No KTP  must contain only digits";
    }
    return null;
  }

  //==============Form commodity entry==============
  String? validateDateSow(String? value) {
    if (value?.isEmpty ?? false) {
      return "Date sow is required";
    }
    return null;
  }

  String? validateAmountSeed(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Amount seed is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Amount seed must contain only digits";
    }
    return null;
  }

  String? validatePriceSeed(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Price seed is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Price seed must contain only digits";
    }
    return null;
  }

  String? validateWeightSeed(String? value) {
    String pattern = r'^(?=\D*(?:\d\D*){1,12}$)\d+(?:\.\d{1,4})?$';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Weight seed is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Weight seed must contain only digits";
    }
    return null;
  }

  String? validateSurvivalRate(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Survival rate is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Survival rate must contain only digits";
    }
    return null;
  }

  String? validateFcr(String? value) {
    String pattern = r'^(?=\D*(?:\d\D*){1,12}$)\d+(?:\.\d{1,4})?$';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Feed conv ratio is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Feed conv ratio must contain only digits";
    }
    return null;
  }

  String? validateFeedAmount(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Feed amount is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Feed amount must contain only digits";
    }
    return null;
  }

  String? validateTargetFish(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Target fish is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Target fish must contain only digits";
    }
    return null;
  }

  String? validateTargetPrice(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Target price is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Target price must contain only digits";
    }
    return null;
  }

  //==============sale entry screen==============
  String? validateharvestWeight(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Harves weight is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Harves weight must contain only digits";
    }
    return null;
  }

  String? validateTotalFishPerKg(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Total fish is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Total fish must contain only digits";
    }
    return null;
  }

  String? validatePricePerKg(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Price per kg is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Price per kg must contain only digits";
    }
    return null;
  }

  String? validateKeterangan(String? value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = RegExp(pattern);
    if (value?.isEmpty ?? false) {
      return "Keterangan is required";
    } else if (value != null && !regExp.hasMatch(value)) {
      return "Keterangan must be a-z and A-Z";
    }
    return null;
  }
}
