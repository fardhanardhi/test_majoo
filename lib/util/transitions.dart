import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class ASharedAxisTransitionSwitcher extends StatelessWidget {
  const ASharedAxisTransitionSwitcher({
    Key? key,
    this.fillColor = Colors.transparent,
    required this.child,
    required this.transitionType,
    this.duration = const Duration(milliseconds: 300),
    this.reverse = false,
  }) : super(key: key);

  final Widget child;
  final Color fillColor;
  final SharedAxisTransitionType transitionType;
  final Duration duration;
  final bool reverse;

  @override
  Widget build(BuildContext context) {
    return PageTransitionSwitcher(
      duration: duration,
      reverse: reverse,
      transitionBuilder: (child, animation, secondaryAnimation) {
        return SharedAxisTransition(
          fillColor: fillColor,
          animation: animation,
          secondaryAnimation: secondaryAnimation,
          transitionType: transitionType,
          child: child,
        );
      },
      child: child,
    );
  }
}

class AFadeThroughTransitionSwitcher extends StatelessWidget {
  const AFadeThroughTransitionSwitcher({
    Key? key,
    this.fillColor = Colors.transparent,
    required this.child,
  }) : super(key: key);

  final Widget child;
  final Color fillColor;

  @override
  Widget build(BuildContext context) {
    return PageTransitionSwitcher(
      transitionBuilder: (child, animation, secondaryAnimation) {
        return FadeThroughTransition(
          fillColor: fillColor,
          animation: animation,
          secondaryAnimation: secondaryAnimation,
          child: child,
        );
      },
      child: child,
    );
  }
}

class AFadeTransitionSwitcher extends StatelessWidget {
  const AFadeTransitionSwitcher({
    Key? key,
    this.fillColor = Colors.transparent,
    required this.child,
  }) : super(key: key);

  final Widget child;
  final Color fillColor;

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 100),
      child: child,
    );
  }
}
