import 'package:flutter/material.dart';

import 'au_color.dart';

class AppTheme {
  bool isDark;

  /// Default constructor
  AppTheme({required this.isDark});

  ThemeData get themeData {
    /// Create a TextTheme and ColorScheme, that we can use to generate ThemeData
    // TextTheme txtThemeScheme =
    //     (isDark ? ThemeData.dark() : ThemeData.light()).textTheme;
    // TextTheme txtThemeStyle = TextTheme(
    //   bodyText1: AUTypo.body1,
    //   bodyText2: AUTypo.body2,
    //   button: AUTypo.button,
    //   caption: AUTypo.caption,
    //   headline1: AUTypo.h1,
    //   headline2: AUTypo.h2,
    //   headline3: AUTypo.h3,
    //   overline: AUTypo.overline,
    //   subtitle1: AUTypo.subtitle1,
    //   subtitle2: AUTypo.subtitle2,
    // );
    // TextTheme txtTheme = txtThemeStyle.merge(txtThemeScheme);
    // Color txtColor = txtTheme.bodyText1.color;
    ColorScheme colorScheme = ColorScheme(
      // Decide how you want to apply your own custom them, to the MaterialApp
      brightness: isDark ? Brightness.dark : Brightness.light,
      primary: AUColor.primary,
      primaryVariant: AUColor.primaryVariant,
      secondary: AUColor.secondary,
      secondaryVariant: AUColor.secondaryVariant,
      background: AUColor.background,
      surface: AUColor.surface,
      onBackground: AUColor.onBackground,
      onSurface: AUColor.onSurface,
      onError: AUColor.onError,
      onPrimary: AUColor.onPrimary,
      onSecondary: AUColor.onSecondary,
      error: AUColor.error,
    );

    /// Now that we have ColorScheme and TextTheme, we can create the ThemeData
    var t = ThemeData.from(colorScheme: colorScheme)
        // We can also add on some extra properties that ColorScheme seems to miss
        .copyWith(
      cardTheme: cardTheme,
    );

    /// Return the themeData which MaterialApp can now use
    return t;
  }

  CardTheme get cardTheme => CardTheme(
        margin: EdgeInsets.zero,
        elevation: 10,
        shadowColor: Colors.black26,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      );
}
