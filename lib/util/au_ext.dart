import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/router.dart';

enum RouteTransition { slide, dualSlide, fade, material, cupertino }

class AUExt {
  static void hideKeyboard(context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static void popScreen(BuildContext context, [dynamic data]) {
    Navigator.pop(context, data);
  }

  static Future pushScreen(
    BuildContext context,
    Widget buildScreen, [
    RouteTransition routeTransition = RouteTransition.slide,
    Widget? fromScreen,
  ]) async {
    dynamic data;
    switch (routeTransition) {
      case RouteTransition.slide:
        data = await Navigator.push(context, ASlideRoute(page: buildScreen));
        break;
      case RouteTransition.fade:
        data = await Navigator.push(context, AFadeRoute(page: buildScreen));
        break;
      case RouteTransition.material:
        data = await Navigator.push(
            context, MaterialPageRoute(builder: (context) => buildScreen));
        break;
      case RouteTransition.dualSlide:
        data = await Navigator.push(
            context,
            ADualSlideRoute(
                enterPage: buildScreen,
                exitPage: fromScreen ?? context.widget));
        break;
      case RouteTransition.cupertino:
        data = await Navigator.push(
            context,
            CupertinoPageRoute(
                fullscreenDialog: true, builder: (context) => buildScreen));
        break;
    }
    return data;
  }

  static void pushAndRemoveScreen(BuildContext context,
      {required Widget pageRef}) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => pageRef),
        (Route<dynamic> route) => false);
  }

  static void showContextSuccessFeedback(
    BuildContext context, {
    required String message,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        backgroundColor: AUColor.primary,
        behavior: SnackBarBehavior.floating,
        action: SnackBarAction(
          label: "Tutup",
          textColor: Colors.white70,
          onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
        ),
      ),
    );
  }

//helper method to show alert dialog
  static showAlertDialog(BuildContext context, String title, String content) {
    // set up the AlertDialog
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
