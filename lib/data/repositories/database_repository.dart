import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseRepository {
  DatabaseRepository.privateConstructor();

  static final DatabaseRepository instance =
      DatabaseRepository.privateConstructor();

  final _databaseName = 'test_database.db';
  final _databaseVersion = 1;

  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await _initDatabase();
      return _database;
    }
  }

  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    // await deleteDatabase(path);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: onCreate);
  }

  Future onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE user(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            username TEXT,
            password TEXT
            )
          ''');

    await db.execute('''
          CREATE TABLE people(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            height TEXT,
            mass TEXT,
            hair_color TEXT,
            skin_color TEXT,
            eye_color TEXT,
            birth_year TEXT,
            gender TEXT,
            homeworld TEXT,
            created DATETIME,
            edited DATETIME,
            url TEXT,
            favorite INTEGER DEFAULT 0
            )
          ''');
  }
}
