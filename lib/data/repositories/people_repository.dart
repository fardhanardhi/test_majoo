import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/api/api.dart';
import 'package:test_majoo/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/data/repositories/database_repository.dart';

class PeopleRepository {
  final dbProvider = DatabaseRepository.instance;
  final ApiProvider _provider = ApiProvider();

  Future<List<People>> showAll(SortPeople sort) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> allRows = await db!.query('people',
        orderBy: sort == SortPeople.asc ? 'name ASC' : 'name DESC');
    List<People> people = allRows.map((item) => People.fromMap(item)).toList();
    return people;
  }

  Future<List<People>> showAllFav() async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> allRows = await db!.query(
      'people',
      where: 'favorite = 1',
    );
    List<People> people = allRows.map((item) => People.fromMap(item)).toList();
    return people;
  }

  Future<People> insert(People people) async {
    final db = await dbProvider.database;
    people.id = await db!.insert(
      'people',
      people.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return people;
  }

  Future<void> insertList(List<People> people) async {
    final db = await dbProvider.database;
    var batch = db!.batch();
    for (var element in people) {
      batch.insert(
        'people',
        element.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
    await batch.commit();
  }

  Future<void> update(People people) async {
    final db = await dbProvider.database;
    await db!.update(
      'people',
      people.toMap(),
      where: "id = ?",
      whereArgs: [people.id],
    );
  }

  Future<void> delete(int id) async {
    final db = await dbProvider.database;
    await db!.delete(
      'people',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<People?> getPeople(int id) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> maps =
        await db!.query('people', where: 'id = ?', whereArgs: [id]);
    if (maps.isNotEmpty) {
      return People.fromMap(maps.first);
    }
    return null;
  }

  Future<void> updateFav(People people) async {
    final db = await dbProvider.database;
    await db!.update(
      'people',
      {'favorite': people.favorite == 0 ? 1 : 0},
      where: "id = ?",
      whereArgs: [people.id],
    );
  }

  Future<List<People>> searchPeople(String keyword, SortPeople sort) async {
    final db = await dbProvider.database;
    final String order = sort == SortPeople.asc ? 'ASC' : 'DESC';
    List<Map<String, dynamic>> maps = await db!.rawQuery(
        "select * from people where name like '%$keyword%' or height like '%$keyword%' or mass like '%$keyword%' or gender like '%$keyword%' or birth_year like '%$keyword%' or eye_color like '%$keyword%' or skin_color like '%$keyword%' or hair_color like '%$keyword%' order by name $order");
    List<People> people = maps.map((item) => People.fromMap(item)).toList();
    return people;
  }

  Future<List<People>> searchPeopleFav(String keyword) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> maps = await db!.rawQuery(
        "select * from people where (name like '%$keyword%' or height like '%$keyword%' or mass like '%$keyword%' or gender like '%$keyword%' or birth_year like '%$keyword%' or eye_color like '%$keyword%' or skin_color like '%$keyword%' or hair_color like '%$keyword%') and favorite=1");
    List<People> people = maps.map((item) => People.fromMap(item)).toList();
    return people;
  }

  Future close() async {
    final db = await dbProvider.database;
    db!.close();
  }

  // fetch api
  Future<PeopleResponse> fetchPeople() async {
    var requestUrl = 'https://swapi.dev/api/people';
    final response = await _provider.get(requestUrl, headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    });
    return PeopleResponse.fromMap(response);
  }

  Future<People> fetchPeopleDetail(String url) async {
    var requestUrl = url;
    final response = await _provider.get(requestUrl, headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    });
    return People.fromMap(response);
  }
}
