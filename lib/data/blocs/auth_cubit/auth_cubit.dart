import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/user.dart';
import 'package:test_majoo/data/repositories/user_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());
  final UserRepository _userRepository = UserRepository();

  Future<void> appStarted() async {
    if (state is AuthInitial) {
      await Future.delayed(const Duration(seconds: 3));
    }
    final bool hasToken = await _userRepository.hasToken();
    emit(AuthLoading());
    if (hasToken) {
      try {
        final String? token = await _userRepository.getToken();
        if (token == null) {
          throw ('tidak ada token');
        }
        final int id = int.parse(token);
        final User? user = await _userRepository.getUser(id);
        if (user != null) {
          emit(AuthAuthenticated(user: user));
        } else {
          emit(
            const AuthUnauthenticated(),
          );
        }
      } catch (error) {
        emit(
          AuthUnauthenticated(
            message: error.toString(),
          ),
        );
      }
    } else {
      emit(
        const AuthUnauthenticated(),
      );
    }
  }

  Future<void> login({
    required String username,
    required String password,
  }) async {
    emit(AuthLoading());
    try {
      final User? user =
          await _userRepository.getUserByUsername(username, password);
      if (user != null) {
        await _userRepository.persistToken(user.id.toString());
        emit(AuthAuthenticated(user: user));
      } else {
        emit(
          const AuthUnauthenticated(
            message: "Register first",
          ),
        );
      }
    } catch (error) {
      emit(
        AuthUnauthenticated(
          message: error.toString(),
        ),
      );
    }
  }

  Future<void> register(
      {required String name,
      required String username,
      required String password}) async {
    emit(AuthLoading());
    try {
      final User? userExist =
          await _userRepository.getUserByUsername(username, password);
      if (userExist == null) {
        final User newUser = await _userRepository.insert(
          User(name: name, username: username, password: password),
        );
        final User? user = await _userRepository.getUser(newUser.id!);
        _userRepository.persistToken(newUser.id.toString());
        emit(AuthAuthenticated(user: user!));
      } else {
        emit(
          const AuthUnauthenticated(
            message: "User already exist",
          ),
        );
      }
    } catch (error) {
      emit(
        AuthUnauthenticated(
          message: error.toString(),
        ),
      );
    }
  }

  Future<void> logout() async {
    await _userRepository.deleteToken();
    emit(
      const AuthUnauthenticated(),
    );
  }
}
