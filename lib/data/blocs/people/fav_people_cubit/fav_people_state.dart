part of 'fav_people_cubit.dart';

abstract class FavPeopleState extends Equatable {
  const FavPeopleState();

  @override
  List<Object?> get props => [];
}

class FavPeopleInitial extends FavPeopleState {}

class FavPeopleLoading extends FavPeopleState {}

class FavPeopleLoaded extends FavPeopleState {
  const FavPeopleLoaded(this.people, [this.mustResetSearch = false]);

  final List<People> people;
  final bool mustResetSearch;

  @override
  List<Object?> get props => [people, mustResetSearch];
}

class FavPeopleLoadFailure extends FavPeopleState {
  final String? message;

  const FavPeopleLoadFailure({this.message});

  @override
  List<Object?> get props => [message];
}
