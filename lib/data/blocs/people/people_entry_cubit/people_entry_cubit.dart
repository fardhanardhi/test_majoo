import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/data/repositories/people_repository.dart';

part 'people_entry_state.dart';

class PeopleEntryCubit extends Cubit<PeopleEntryState> {
  PeopleEntryCubit() : super(PeopleEntryInitial());

  final PeopleRepository _peopleRepository = PeopleRepository();

  Future<void> insert({required People people}) async {
    emit(PeopleEntryLoading());
    try {
      await _peopleRepository.insert(people);
      emit(PeopleEntrySuccess());
    } catch (error) {
      emit(PeopleEntryFailure(message: error.toString()));
    }
  }

  Future<void> update({required People people}) async {
    emit(PeopleEntryLoading());
    try {
      await _peopleRepository.update(people);
      emit(PeopleEntrySuccess());
    } catch (error) {
      emit(PeopleEntryFailure(message: error.toString()));
    }
  }
}
