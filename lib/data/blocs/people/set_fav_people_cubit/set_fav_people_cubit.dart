import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/data/repositories/people_repository.dart';

part 'set_fav_people_state.dart';

class SetFavPeopleCubit extends Cubit<SetFavPeopleState> {
  SetFavPeopleCubit() : super(SetFavPeopleInitial());

  final PeopleRepository _peopleRepository = PeopleRepository();

  Future<void> updateFav({required People people}) async {
    emit(SetFavPeopleLoading());
    try {
      await _peopleRepository.updateFav(people);
      emit(SetFavPeopleSuccess());
    } catch (error) {
      emit(SetFavPeopleFailure(message: error.toString()));
    }
  }
}
