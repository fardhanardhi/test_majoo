import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/data/repositories/people_repository.dart';

part 'people_detail_state.dart';

class PeopleDetailCubit extends Cubit<PeopleDetailState> {
  PeopleDetailCubit() : super(PeopleDetailInitial());

  final PeopleRepository _peopleRepository = PeopleRepository();

  Future<void> load(int id) async {
    emit(PeopleDetailLoading());
    try {
      final response = await _peopleRepository.getPeople(id);
      emit(PeopleDetailLoaded(response));
    } catch (error) {
      // print(error.toString());
      emit(PeopleDetailLoadFailure(message: error.toString()));
    }
  }

  Future<void> reload(int id) async {
    try {
      final response = await _peopleRepository.getPeople(id);
      emit(PeopleDetailLoaded(response));
    } catch (error) {
      // print(error.toString());
      emit(PeopleDetailLoadFailure(message: error.toString()));
    }
  }
}
