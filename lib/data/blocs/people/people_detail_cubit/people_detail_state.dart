part of 'people_detail_cubit.dart';

abstract class PeopleDetailState extends Equatable {
  const PeopleDetailState();

  @override
  List<Object?> get props => [];
}

class PeopleDetailInitial extends PeopleDetailState {}

class PeopleDetailLoading extends PeopleDetailState {}

class PeopleDetailLoaded extends PeopleDetailState {
  const PeopleDetailLoaded(this.people);

  final People? people;

  @override
  List<Object?> get props => [people];
}

class PeopleDetailLoadFailure extends PeopleDetailState {
  final String? message;

  const PeopleDetailLoadFailure({this.message});

  @override
  List<Object?> get props => [message];
}
