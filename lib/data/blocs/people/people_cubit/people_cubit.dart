import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/data/repositories/people_repository.dart';

part 'people_state.dart';

enum ViewPeople { grid, list }
enum SortPeople { asc, desc, none }

class PeopleCubit extends Cubit<PeopleState> {
  PeopleCubit() : super(PeopleInitial());
  ViewPeople viewPeople = ViewPeople.list;
  SortPeople sortPeople = SortPeople.asc;
  List<People> people = [];

  final PeopleRepository _peopleRepository = PeopleRepository();

  void setView() {
    viewPeople =
        viewPeople == ViewPeople.list ? ViewPeople.grid : ViewPeople.list;
    emit(PeopleLoading());
    emit(
      PeopleLoaded(
        people,
        viewPeople,
        sortPeople,
      ),
    );
  }

  Future<void> sort() async {
    sortPeople =
        sortPeople == SortPeople.asc ? SortPeople.desc : SortPeople.asc;
    // emit(PeopleLoading());
    await _showAllLocal();
  }

  Future<void> load() async {
    emit(PeopleLoading());
    try {
      final userDataLocal = await _peopleRepository.showAll(sortPeople);
      if (userDataLocal.isNotEmpty) {
        people = userDataLocal;
        emit(PeopleLoaded(userDataLocal, viewPeople, sortPeople));
      } else {
        final response = await _peopleRepository.fetchPeople();
        if (response.results.isNotEmpty) {
          await _peopleRepository.insertList(response.results);
          await showAllLocalData();
        } else {
          emit(PeopleLoaded(people, viewPeople, sortPeople));
        }
      }
    } catch (error) {
      emit(PeopleLoadFailure(message: error.toString()));
    }
  }

  Future<void> showAllLocalData() async {
    // emit(PeopleLoading());
    await _showAllLocal();
  }

  Future<void> _showAllLocal() async {
    try {
      final userDataLocal = await _peopleRepository.showAll(sortPeople);
      people = userDataLocal;
      emit(PeopleLoaded(userDataLocal, viewPeople, sortPeople));
    } catch (error) {
      emit(PeopleLoadFailure(message: error.toString()));
    }
  }

  Future<void> reloadLocalData({bool mustResetSearch = false}) async {
    try {
      final userDataLocal = await _peopleRepository.showAll(sortPeople);
      people = userDataLocal;
      emit(
          PeopleLoaded(userDataLocal, viewPeople, sortPeople, mustResetSearch));
    } catch (error) {
      emit(PeopleLoadFailure(message: error.toString()));
    }
  }

  Future<void> search(String keyword) async {
    // emit(PeopleLoading());
    try {
      final userDataLocal =
          await _peopleRepository.searchPeople(keyword, sortPeople);
      people = userDataLocal;
      emit(PeopleLoaded(userDataLocal, viewPeople, sortPeople));
    } catch (error) {
      emit(PeopleLoadFailure(message: error.toString()));
    }
  }
}
