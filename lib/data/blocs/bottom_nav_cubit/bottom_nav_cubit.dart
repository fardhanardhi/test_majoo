import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';

part 'bottom_nav_state.dart';

class BottomNavCubit extends Cubit<int> {
  BottomNavCubit() : super(0);

  void navItemTapped(int index) {
    emit(index);
  }

  List<BottomNavItem> get navItem {
    return [
      BottomNavItem(
        icon: const Icon(
          Boxicons.bx_home,
        ),
        activeIcon: const Icon(
          Boxicons.bxs_home,
        ),
        label: "Home",
      ),
      BottomNavItem(
        icon: const Icon(
          Boxicons.bx_heart,
        ),
        activeIcon: const Icon(
          Boxicons.bxs_heart,
        ),
        label: "Favorite",
      ),
      BottomNavItem(
        icon: const Icon(
          Boxicons.bx_user,
        ),
        activeIcon: const Icon(
          Boxicons.bxs_user,
        ),
        label: "Profile",
      ),
    ];
  }
}

class BottomNavItem {
  BottomNavItem({
    required this.icon,
    required this.activeIcon,
    required this.label,
    this.onTaped,
  });

  final Widget icon;
  final Widget activeIcon;
  final String label;
  final VoidCallback? onTaped;
}
