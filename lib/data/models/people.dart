class PeopleResponse {
  PeopleResponse({
    required this.count,
    required this.next,
    required this.previous,
    required this.results,
  });

  int count;
  String next;
  dynamic previous;
  List<People> results;

  PeopleResponse copyWith({
    int? count,
    String? next,
    String? previous,
    List<People>? results,
  }) =>
      PeopleResponse(
        count: count ?? this.count,
        next: next ?? this.next,
        previous: previous ?? this.previous,
        results: results ?? this.results,
      );

  factory PeopleResponse.fromMap(Map<String, dynamic> map) => PeopleResponse(
        count: map["count"],
        next: map["next"],
        previous: map["previous"],
        results: map["results"].length == 0
            ? []
            : List<People>.from(map["results"].map((x) => People.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toMap())),
      };
}

class People {
  People({
    this.id,
    required this.name,
    required this.height,
    required this.mass,
    required this.hairColor,
    required this.skinColor,
    required this.eyeColor,
    required this.birthYear,
    required this.gender,
    this.homeworld,
    this.films,
    this.species,
    this.vehicles,
    this.starships,
    required this.created,
    required this.edited,
    this.url,
    required this.favorite,
  });

  int? id;
  String name;
  String height;
  String mass;
  String hairColor;
  String skinColor;
  String eyeColor;
  String birthYear;
  String gender;
  String? homeworld;
  List<String>? films;
  List<String>? species;
  List<String>? vehicles;
  List<String>? starships;
  DateTime created;
  DateTime edited;
  String? url;
  int favorite;

  People copyWith({
    int? id,
    String? name,
    String? height,
    String? mass,
    String? hairColor,
    String? skinColor,
    String? eyeColor,
    String? birthYear,
    String? gender,
    String? homeworld,
    List<String>? films,
    List<String>? species,
    List<String>? vehicles,
    List<String>? starships,
    DateTime? created,
    DateTime? edited,
    String? url,
    int? favorite,
  }) =>
      People(
        id: id ?? this.id,
        name: name ?? this.name,
        height: height ?? this.height,
        mass: mass ?? this.mass,
        hairColor: hairColor ?? this.hairColor,
        skinColor: skinColor ?? this.skinColor,
        eyeColor: eyeColor ?? this.eyeColor,
        birthYear: birthYear ?? this.birthYear,
        gender: gender ?? this.gender,
        homeworld: homeworld ?? this.homeworld,
        films: films ?? this.films,
        species: species ?? this.species,
        vehicles: vehicles ?? this.vehicles,
        starships: starships ?? this.starships,
        created: created ?? this.created,
        edited: edited ?? this.edited,
        url: url ?? this.url,
        favorite: favorite ?? this.favorite,
      );

  factory People.fromMap(Map<String, dynamic> map) => People(
        id: map["id"],
        name: map["name"],
        height: map["height"],
        mass: map["mass"],
        hairColor: map["hair_color"],
        skinColor: map["skin_color"],
        eyeColor: map["eye_color"],
        birthYear: map["birth_year"],
        gender: map["gender"],
        homeworld: map["homeworld"],
        films: map["films"] == null
            ? null
            : List<String>.from(map["films"].map((x) => x)),
        species: map["species"] == null
            ? null
            : List<String>.from(map["species"].map((x) => x)),
        vehicles: map["vehicles"] == null
            ? null
            : List<String>.from(map["vehicles"].map((x) => x)),
        starships: map["starships"] == null
            ? null
            : List<String>.from(map["starships"].map((x) => x)),
        created: DateTime.parse(map["created"]),
        edited: DateTime.parse(map["edited"]),
        url: map["url"],
        favorite: map["favorite"] ?? 0,
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "height": height,
        "mass": mass,
        "hair_color": hairColor,
        "skin_color": skinColor,
        "eye_color": eyeColor,
        "birth_year": birthYear,
        "gender": gender,
        "homeworld": homeworld,
        "created": created.toIso8601String(),
        "edited": edited.toIso8601String(),
        "url": url,
        "favorite": favorite,
      };

  bool get isFav => favMap[favorite] ?? false;

  Map<int, bool> favMap = {
    1: true,
    0: false,
  };
}
