import 'package:flutter/material.dart';
import 'package:test_majoo/util/au_ext.dart';

class ConfirmationDialog {
  static Future show(BuildContext context,
      {required String desc, required Function() onYes}) async {
    AUExt.hideKeyboard(context);
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)), //this right here
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    desc,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.pop(context);
                              onYes();
                            },
                            child: const Text('Save')),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: OutlinedButton(
                          onPressed: () => Navigator.pop(context),
                          child: const Text('No'),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
    return;
  }
}
