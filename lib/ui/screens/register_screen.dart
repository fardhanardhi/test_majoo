import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/au_ext.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<RegisterScreen> {
  late TextEditingController _nameCtrl, _unameCtrl, _passCtrl;

  @override
  void initState() {
    super.initState();
    _nameCtrl = TextEditingController(text: '');
    _unameCtrl = TextEditingController(text: '');
    _passCtrl = TextEditingController(text: '');
  }

  @override
  void dispose() {
    _nameCtrl.dispose();
    _unameCtrl.dispose();
    _passCtrl.dispose();
    super.dispose();
  }

  void _handleRegister() async {
    await BlocProvider.of<AuthCubit>(context).register(
      name: _nameCtrl.text,
      username: _unameCtrl.text,
      password: _passCtrl.text,
    );
    AUExt.popScreen(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AuthUnauthenticated) {
          if (state.message != null) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.message}'),
                backgroundColor: AUColor.primaryText,
              ),
            );
          }
          return;
        }
      },
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light
            .copyWith(statusBarColor: Colors.transparent),
        child: Scaffold(
          body: Stack(
            children: [
              Container(
                color: AUColor.primary,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                    color: AUColor.surface,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(20)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        TextField(
                          controller: _nameCtrl,
                          decoration: getInputDecoration('Name'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _unameCtrl,
                          decoration: getInputDecoration('Username'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _passCtrl,
                          decoration: getInputDecoration('Password'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          onPressed: _handleRegister,
                          child: const Text('Register'),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration getInputDecoration([String? name]) {
    var border = OutlineInputBorder(borderRadius: BorderRadius.circular(7.5));
    return InputDecoration(
      border: border,
      enabledBorder: border,
      focusedBorder: border.copyWith(borderSide: const BorderSide(width: 2)),
      labelText: name,
    );
  }
}
