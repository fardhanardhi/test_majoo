import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_majoo/data/blocs/bottom_nav_cubit/bottom_nav_cubit.dart';
import 'package:test_majoo/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_majoo/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_majoo/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_majoo/ui/screens/screens.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/transitions.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with WidgetsBindingObserver {
  // final GlobalKey<ScaffoldState> _scaffoldRootKey = GlobalKey<ScaffoldState>();
  var _isKeyboardVisible =
      WidgetsBinding.instance!.window.viewInsets.bottom > 0.0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance!.window.viewInsets.bottom;
    final newValue = bottomInset > 0.0;
    if (newValue != _isKeyboardVisible) {
      setState(() {
        _isKeyboardVisible = newValue;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final BottomNavCubit _bottomNavCubit =
        BlocProvider.of<BottomNavCubit>(context);
    final double _statusBarHeight = MediaQuery.of(context).padding.top;

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Colors.transparent),
      child: Scaffold(
        body: BlocBuilder<BottomNavCubit, int>(
          bloc: _bottomNavCubit,
          builder: (context, state) => Stack(
            children: [
              ASharedAxisTransitionSwitcher(
                transitionType: SharedAxisTransitionType.vertical,
                reverse: state == 2,
                child: state == 0
                    ? const HomeScreen()
                    : state == 1
                        ? const FavoriteScreen()
                        : state == 2
                            ? const ProfileScreen()
                            : const SizedBox.shrink(),
              ),
              Wrap(
                children: [
                  ASharedAxisTransitionSwitcher(
                    transitionType: SharedAxisTransitionType.vertical,
                    reverse: state != 2,
                    child: state == 2
                        ? const SizedBox()
                        : Stack(
                            children: [
                              Container(
                                height: 100,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment
                                        .bottomCenter, // 10% of the width, so there are ten blinds.
                                    colors: <Color>[
                                      Colors.white,
                                      Colors.white,
                                      Colors.white.withOpacity(0),
                                      // Colors.transparent,
                                    ], // red to yellow
                                    tileMode: TileMode
                                        .repeated, // repeats the gradient over the canvas
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topCenter,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                          horizontal: 15.0)
                                      .copyWith(top: _statusBarHeight + 10),
                                  child: const SearchBar(),
                                ),
                              ),
                            ],
                          ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Wrap(
                  children: [
                    ASharedAxisTransitionSwitcher(
                      duration: Duration(milliseconds: 500),
                      transitionType: SharedAxisTransitionType.vertical,
                      reverse: _isKeyboardVisible,
                      child: _isKeyboardVisible
                          ? const SizedBox()
                          : Stack(
                              key: ValueKey("value"),
                              children: [
                                Positioned.fill(
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment
                                              .bottomCenter, // 10% of the width, so there are ten blinds.
                                          colors: <Color>[
                                            Colors.white.withOpacity(0),
                                            Colors.white,
                                            // Colors.transparent,
                                          ], // red to yellow
                                          tileMode: TileMode
                                              .repeated, // repeats the gradient over the canvas
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                const Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Padding(
                                    padding: EdgeInsets.all(25),
                                    child: NavigationPanel(),
                                  ),
                                )
                              ],
                            ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NavigationPanel extends StatelessWidget {
  const NavigationPanel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _bottomNavCubit = BlocProvider.of<BottomNavCubit>(context);
    final _item = _bottomNavCubit.navItem;

    return Container(
      decoration: BoxDecoration(
        color: AUColor.primary,
        borderRadius: BorderRadius.circular(50),
        boxShadow: [
          BoxShadow(
            blurRadius: 30,
            offset: const Offset(0, 10),
            color: Colors.black.withOpacity(0.3),
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Material(
          color: AUColor.infoColor,
          type: MaterialType.transparency,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
            child: BlocBuilder<BottomNavCubit, int>(
              builder: (context, state) {
                return Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    for (var i = 0; i < _item.length; i++) ...[
                      if (i != 0)
                        const SizedBox(
                          width: 25,
                        ),
                      IconButton(
                        color: state == i
                            ? AUColor.onPrimary
                            : AUColor.onPrimary.withOpacity(0.5),
                        splashRadius: 60,
                        highlightColor: AUColor.onPrimary.withOpacity(0.1),
                        splashColor: AUColor.onPrimary.withOpacity(0.3),
                        iconSize: 25,
                        tooltip: _item[i].label,
                        onPressed: () {
                          _bottomNavCubit.navItemTapped(i);
                        },
                        icon: state == i ? _item[i].activeIcon : _item[i].icon,
                      ),
                    ]
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class SearchBar extends StatefulWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  late TextEditingController _searchController;
  Timer? _debounce;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController(text: '');
  }

  @override
  void dispose() {
    _searchController.dispose();
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    super.dispose();
  }

  _onSearchChanged(
      String keyword, PeopleCubit peopleCubit, FavPeopleCubit favPeopleCubit) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (keyword.isNotEmpty) {
        peopleCubit.search(keyword.toString());
        favPeopleCubit.searchFav(keyword.toString());
      } else {
        peopleCubit.showAllLocalData();
        favPeopleCubit.showAllFavData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final PeopleCubit _peopleCubit = BlocProvider.of<PeopleCubit>(context);
    final FavPeopleCubit _favPeopleCubit =
        BlocProvider.of<FavPeopleCubit>(context);
    final BottomNavCubit _bottomNavCubit =
        BlocProvider.of<BottomNavCubit>(context);

    return MultiBlocListener(
      listeners: [
        BlocListener<BottomNavCubit, int>(
          listener: (context, state) {
            _searchController.clear();
          },
        ),
        BlocListener<FavPeopleCubit, FavPeopleState>(
          listener: (context, state) {
            if (state is FavPeopleLoaded && state.mustResetSearch) {
              _searchController.clear();
            }
          },
        ),
        BlocListener<PeopleCubit, PeopleState>(
          listener: (context, state) {
            if (state is PeopleLoaded && state.mustResetSearch) {
              _searchController.clear();
            }
          },
        ),
      ],
      child: BlocBuilder<PeopleCubit, PeopleState>(
        builder: (context, state) {
          return Container(
            decoration: BoxDecoration(
              color: AUColor.surface,
              borderRadius: BorderRadius.circular(50),
              border: Border.all(width: 2),
              boxShadow: [
                BoxShadow(
                  blurRadius: 30,
                  // offset: Offset(0, 5),
                  color: Colors.black.withOpacity(0.15),
                ),
              ],
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Material(
                type: MaterialType.transparency,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(left: 15, right: 7),
                      child: Icon(
                        Boxicons.bx_search,
                      ),
                    ),
                    // Text(
                    //   'Search peoples',
                    //   style: Theme.of(context).textTheme.subtitle1,
                    // ),
                    Expanded(
                      child: TextField(
                        controller: _searchController,
                        onChanged: (val) => _onSearchChanged(
                            val, _peopleCubit, _favPeopleCubit),
                        decoration: const InputDecoration(
                          hintText: "Search peoples",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 5),
                      child: BlocBuilder<BottomNavCubit, int>(
                        builder: (context, navState) {
                          return AFadeThroughTransitionSwitcher(
                            child: navState == 1
                                ? const SizedBox()
                                : IconButton(
                                    // splashRadius: 50,
                                    visualDensity: VisualDensity.compact,
                                    padding: EdgeInsets.zero,
                                    onPressed: () {
                                      if (_searchController.text.isNotEmpty) {
                                        _searchController.text = '';
                                        _onSearchChanged(
                                            '', _peopleCubit, _favPeopleCubit);
                                      } else {
                                        _peopleCubit.setView();
                                      }
                                    },
                                    iconSize: 27,
                                    icon: AFadeThroughTransitionSwitcher(
                                      child: _searchController.text.isNotEmpty
                                          ? const Icon(
                                              Boxicons.bx_x,
                                              size: 30,
                                            )
                                          : AFadeTransitionSwitcher(
                                              child: state is PeopleLoaded &&
                                                      state.view ==
                                                          ViewPeople.list
                                                  ? const Icon(
                                                      Icons.grid_view,
                                                      key: ValueKey("g"),
                                                    )
                                                  : const Icon(
                                                      Icons
                                                          .view_agenda_outlined,
                                                      key: ValueKey("l"),
                                                    ),
                                            ),
                                    ),
                                  ),
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5)
                          .copyWith(right: 10),
                      child: IconButton(
                        // splashRadius: 50,
                        visualDensity: VisualDensity.compact,
                        padding: EdgeInsets.zero,
                        onPressed: () => _bottomNavCubit.navItemTapped(2),
                        iconSize: 30,
                        icon: const Icon(
                          Boxicons.bxs_user_circle,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
