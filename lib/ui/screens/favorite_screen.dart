import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_majoo/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_majoo/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_majoo/ui/screens/screens.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/au_ext.dart';
import 'package:test_majoo/util/transitions.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  late FavPeopleCubit _favPeopleCubit;
  late SetFavPeopleCubit _setFavPeopleCubit;

  @override
  void initState() {
    super.initState();
    _favPeopleCubit = BlocProvider.of<FavPeopleCubit>(context)
      ..showAllFavData();
    _setFavPeopleCubit = SetFavPeopleCubit();
  }

  @override
  void dispose() {
    _setFavPeopleCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => _favPeopleCubit,
        ),
        BlocProvider(
          create: (_) => _setFavPeopleCubit,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            bloc: _setFavPeopleCubit,
            listener: (context, state) async {
              if (state is SetFavPeopleFailure) {
                if (state.message != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Failure add people to favorite'),
                      backgroundColor: AUColor.primaryText,
                    ),
                  );
                }
                return;
              }
              if (state is SetFavPeopleSuccess) {
                await _favPeopleCubit.reloadFavLocalData(mustResetSearch: true);
                return;
              }
            },
          ),
        ],
        child: GestureDetector(
          onTap: () => AUExt.hideKeyboard(context),
          child: SafeArea(
            child: Scaffold(
              body: BlocBuilder<FavPeopleCubit, FavPeopleState>(
                bloc: _favPeopleCubit,
                builder: (context, state) => ASharedAxisTransitionSwitcher(
                  transitionType: SharedAxisTransitionType.vertical,
                  fillColor: Colors.transparent,
                  child: state is FavPeopleLoading
                      ? const Center(
                          key: ValueKey('load'),
                          child: CircularProgressIndicator(),
                        )
                      : state is FavPeopleLoaded
                          ? ASharedAxisTransitionSwitcher(
                              transitionType: SharedAxisTransitionType.vertical,
                              child: state.people.isNotEmpty
                                  ? ListView.separated(
                                      padding: const EdgeInsets.fromLTRB(
                                        0,
                                        75,
                                        0,
                                        120,
                                      ),
                                      itemBuilder: (context, index) {
                                        var _peopleItem = state.people[index];
                                        String _name = _peopleItem
                                            .name.characters
                                            .split(Characters(" "))
                                            .map((e) => e.first)
                                            .join()
                                            .padRight(5)
                                            .substring(0, 2)
                                            .trim();
                                        return Padding(
                                            padding:
                                                const EdgeInsets.only(left: 5),
                                            child:
                                                AFadeThroughTransitionSwitcher(
                                              child: OpenContainer(
                                                openShape:
                                                    const RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.vertical(
                                                          bottom:
                                                              Radius.circular(
                                                                  30)),
                                                ),
                                                closedElevation: 0,
                                                closedShape:
                                                    const RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.horizontal(
                                                          left: Radius.circular(
                                                              50)),
                                                ),
                                                closedBuilder:
                                                    (context, openContainer) =>
                                                        ListTile(
                                                  key: ValueKey(_peopleItem.id),
                                                  shape:
                                                      const RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.horizontal(
                                                      left: Radius.circular(50),
                                                    ),
                                                  ),
                                                  contentPadding:
                                                      const EdgeInsets
                                                          .symmetric(
                                                    horizontal: 20,
                                                  ).copyWith(left: 15),
                                                  leading: CircleAvatar(
                                                    backgroundColor:
                                                        AUColor.primary,
                                                    foregroundColor:
                                                        AUColor.onPrimary,
                                                    child: Text(_name),
                                                  ),
                                                  trailing: IconButton(
                                                    color: AUColor.primary
                                                        .withOpacity(0.7),
                                                    onPressed: () async =>
                                                        await _setFavPeopleCubit
                                                            .updateFav(
                                                                people:
                                                                    _peopleItem),
                                                    icon:
                                                        ASharedAxisTransitionSwitcher(
                                                      transitionType: _peopleItem
                                                              .isFav
                                                          ? SharedAxisTransitionType
                                                              .vertical
                                                          : SharedAxisTransitionType
                                                              .scaled,
                                                      child: _peopleItem.isFav
                                                          ? const Icon(
                                                              Boxicons
                                                                  .bxs_heart,
                                                              key: ValueKey(
                                                                  "fav"),
                                                            )
                                                          : const Icon(
                                                              Boxicons.bx_heart,
                                                              key: ValueKey(
                                                                  "no"),
                                                            ),
                                                    ),
                                                  ),
                                                  onTap: () {
                                                    FocusManager
                                                        .instance.primaryFocus
                                                        ?.unfocus();
                                                    openContainer();
                                                  },
                                                  title: Text(
                                                    state.people[index].name,
                                                  ),
                                                  subtitle: Text(state
                                                      .people[index].gender),
                                                ),
                                                openBuilder: (context, tes) =>
                                                    PeopleDetailScreen(
                                                        people: state
                                                            .people[index]),
                                              ),
                                            ));
                                      },
                                      separatorBuilder: (context, index) =>
                                          const SizedBox(),
                                      itemCount: state.people.length)
                                  : const Center(
                                      key: ValueKey('empty'),
                                      child: Text("Empty Data"),
                                    ),
                            )
                          : const Center(
                              key: ValueKey('fail'),
                              child: Text("Load Data Failure"),
                            ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
