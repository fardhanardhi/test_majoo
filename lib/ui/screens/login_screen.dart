import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_majoo/ui/screens/screens.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/au_ext.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _unameCtrl, _passCtrl;

  @override
  void initState() {
    super.initState();
    _unameCtrl = TextEditingController(text: '');
    _passCtrl = TextEditingController(text: '');
  }

  @override
  void dispose() {
    _unameCtrl.dispose();
    _passCtrl.dispose();
    super.dispose();
  }

  void _handleLogin() async {
    await BlocProvider.of<AuthCubit>(context).login(
      username: _unameCtrl.text,
      password: _passCtrl.text,
    );
    // AUExt.popScreen(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AuthUnauthenticated) {
          if (state.message != null) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.message}'),
                backgroundColor: AUColor.primaryText,
              ),
            );
          }
          return;
        }
        // if (state is AuthAuthenticated) {
        //   AUExt.popUntilRoot(context);
        //   return;
        // }
      },
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light
            .copyWith(statusBarColor: Colors.transparent),
        child: Scaffold(
          body: Stack(
            children: [
              Container(
                color: AUColor.primary,
              ),
              Container(
                // height: 100,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment
                        .bottomCenter, // 10% of the width, so there are ten blinds.
                    colors: <Color>[
                      AUColor.onSurface,
                      // Colors.white,
                      Colors.black.withOpacity(0),
                      Colors.black.withOpacity(0),
                      // Colors.transparent,
                    ], // red to yellow
                    tileMode: TileMode
                        .repeated, // repeats the gradient over the canvas
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 100),
                child: Center(
                    child: CircleAvatar(
                  backgroundColor: Colors.black,
                  radius: 55,
                  child: Text(
                    'm',
                    style: Theme.of(context).textTheme.headline1!.copyWith(
                          color: AUColor.onPrimary,
                        ),
                  ),
                )),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                    color: AUColor.surface,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(20)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        TextField(
                          controller: _unameCtrl,
                          decoration: getInputDecoration('Username'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _passCtrl,
                          decoration: getInputDecoration('Password'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          onPressed: _handleLogin,
                          child: const Text('Login'),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Center(
                          child: GestureDetector(
                            onTap: () => AUExt.pushScreen(
                              context,
                              const RegisterScreen(),
                              RouteTransition.fade,
                            ),
                            child: const Text("Register"),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration getInputDecoration([String? name]) {
    var border = OutlineInputBorder(borderRadius: BorderRadius.circular(7.5));
    return InputDecoration(
      border: border,
      enabledBorder: border,
      focusedBorder: border.copyWith(borderSide: const BorderSide(width: 2)),
      labelText: name,
    );
  }
}
