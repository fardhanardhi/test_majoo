// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_majoo/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_majoo/data/blocs/people/people_entry_cubit/people_entry_cubit.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/ui/widgets/confirmation_dialog.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/au_ext.dart';
import 'package:test_majoo/util/validator.dart';

class PeopleEntryScreen extends StatefulWidget {
  const PeopleEntryScreen({Key? key, this.people}) : super(key: key);

  final People? people;

  @override
  _PeopleEntryScreenState createState() => _PeopleEntryScreenState();
}

class _PeopleEntryScreenState extends State<PeopleEntryScreen> {
  final AValidator _v = AValidator();
  final _formKey = GlobalKey<FormState>();
  late PeopleEntryCubit _peopleEntryCubit;
  late TextEditingController _nameController,
      _heightController,
      _massController,
      _birthController,
      _eyeController,
      _hairController,
      _genderController,
      _skinController;

  bool get _isUpdateEntry => widget.people != null;
  // late String _selectedGender;

  @override
  void initState() {
    super.initState();
    _genderController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.gender.toLowerCase()}' : '');
    _peopleEntryCubit = PeopleEntryCubit();
    _nameController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.name}' : '');
    _heightController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.height}' : '');
    _massController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.mass}' : '');
    _birthController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.birthYear}' : '');
    _eyeController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.eyeColor}' : '');
    _hairController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.hairColor}' : '');
    _skinController = TextEditingController(
        text: _isUpdateEntry ? '${widget.people?.skinColor}' : '');
  }

  void _handleSave() async {
    AUExt.hideKeyboard(context);
    if (_formKey.currentState?.validate() ?? false) {
      if (!_isUpdateEntry) {
        await _peopleEntryCubit.insert(
          people: People(
            name: _nameController.text,
            height: _heightController.text,
            mass: _massController.text,
            hairColor: _hairController.text,
            skinColor: _skinController.text,
            eyeColor: _eyeController.text,
            gender: _genderController.text,
            birthYear: _birthController.text,
            created: DateTime.now(),
            edited: DateTime.now(),
            favorite: 0,
          ),
        );
      } else {
        await _peopleEntryCubit.update(
          people: People(
            id: widget.people?.id,
            name: _nameController.text,
            height: _heightController.text,
            mass: _massController.text,
            hairColor: _hairController.text,
            skinColor: _skinController.text,
            eyeColor: _eyeController.text,
            gender: _genderController.text,
            birthYear: _birthController.text,
            created: widget.people?.created ?? DateTime.now(),
            edited: DateTime.now(),
            favorite: widget.people?.favorite ?? 0,
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _peopleEntryCubit,
      child: BlocListener(
        bloc: _peopleEntryCubit,
        listener: (context, state) async {
          if (state is PeopleEntryFailure) {
            if (state.message != null) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(
                        "${_isUpdateEntry ? 'Edit People' : 'Add People'} Failure"),
                  ),
                );
            }
            return;
          }
          if (state is PeopleEntrySuccess) {
            await BlocProvider.of<PeopleCubit>(context).showAllLocalData();
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  margin: EdgeInsets.fromLTRB(15, 5, 15, 130),
                  behavior: SnackBarBehavior.floating,
                  action: SnackBarAction(
                    textColor: AUColor.onPrimary,
                    label: "Dismiss",
                    onPressed: () {},
                  ),
                  content: Text(
                      "${_isUpdateEntry ? 'Edit People' : 'Add People'} Success"),
                ),
              );
            AUExt.popScreen(context, true);
            return;
          }
        },
        child: GestureDetector(
          onTap: () => AUExt.hideKeyboard(context),
          child: Scaffold(
            appBar: AppBar(
              title: Text(!_isUpdateEntry ? "Add People " : "Edit People"),
              systemOverlayStyle: SystemUiOverlayStyle.dark,
              backgroundColor: AUColor.surface,
              foregroundColor: AUColor.onSurface,
              actions: [
                TextButton(
                  onPressed: _isUpdateEntry
                      ? () => ConfirmationDialog.show(
                            context,
                            desc:
                                "Are you sure data is correct and want to change '${widget.people?.name}' data? ",
                            onYes: () => _handleSave(),
                          )
                      : _handleSave,
                  child: Text('Save'),
                ),
              ],
              shape: Border(
                bottom: BorderSide(
                  width: 3,
                ),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: _v.validateRequired,
                        controller: _nameController,
                        decoration: getInputDecoration('Name'),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: _v.validateRequired,
                                  keyboardType: TextInputType.number,
                                  controller: _heightController,
                                  decoration: getInputDecoration('Height'),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: _v.validateRequired,
                                  keyboardType: TextInputType.number,
                                  controller: _massController,
                                  decoration: getInputDecoration('Mass'),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Stack(
                              children: [
                                AbsorbPointer(
                                  child: TextFormField(
                                    controller: _genderController,
                                    validator: _v.validateRequired,
                                    style: const TextStyle(
                                        color: Colors.transparent),
                                    decoration:
                                        getInputDecoration('Gender').copyWith(
                                      floatingLabelBehavior:
                                          FloatingLabelBehavior.always,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 59,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          borderRadius:
                                              const BorderRadius.horizontal(
                                            left: Radius.circular(10),
                                          ),
                                          onTap: () => setState(() =>
                                              _genderController.text = 'male'),
                                          child: Icon(
                                            Boxicons.bx_male_sign,
                                            color: _genderController.text
                                                        .toLowerCase() ==
                                                    'male'
                                                ? Colors.black
                                                : Colors.grey,
                                            size: 30,
                                          ),
                                        ),
                                      ),
                                      const VerticalDivider(
                                        color: Colors.black,
                                        width: 0,
                                        thickness: 1,
                                      ),
                                      Expanded(
                                        child: InkWell(
                                          borderRadius:
                                              const BorderRadius.horizontal(
                                            right: Radius.circular(10),
                                          ),
                                          onTap: () => setState(() =>
                                              _genderController.text =
                                                  'female'),
                                          child: Icon(
                                            Boxicons.bx_female_sign,
                                            color: _genderController.text
                                                        .toLowerCase() ==
                                                    'female'
                                                ? Colors.black
                                                : Colors.grey,
                                            size: 30,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: _v.validateRequired,
                                  keyboardType: TextInputType.visiblePassword,
                                  controller: _birthController,
                                  decoration: getInputDecoration('Birth Year'),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: _v.validateRequired,
                        controller: _eyeController,
                        decoration: getInputDecoration('Eye Color'),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: _v.validateRequired,
                        controller: _hairController,
                        decoration: getInputDecoration('Hair Color'),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: _v.validateRequired,
                        controller: _skinController,
                        decoration: getInputDecoration('Skin Color'),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  InputDecoration getInputDecoration([String? name]) {
    var border = OutlineInputBorder(borderRadius: BorderRadius.circular(7.5));
    return InputDecoration(
      border: border,
      enabledBorder: border,
      focusedBorder: border.copyWith(borderSide: const BorderSide(width: 2)),
      labelText: name,
    );
  }
}
