import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_majoo/util/au_color.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late double _indicatorOpacity;
  late Timer _opacityTimer;

  @override
  void initState() {
    _indicatorOpacity = 0.0;
    super.initState();
    _startAnimate();
    // Future.delayed(Duration(milliseconds: 300), () {
    //   _indicatorOpacity = 1;
    // });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<Timer> _startAnimate() async {
    var duration = Duration(milliseconds: 300);
    _opacityTimer = Timer(duration, () async {
      setState(() {
        _indicatorOpacity = 1;
      });
    });
    return _opacityTimer;
  }

  @override
  Widget build(BuildContext context) {
    final double _screenWidth = MediaQuery.of(context).size.width;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent,
      ),
      child: GestureDetector(
        onTap: () {
          setState(() {
            _indicatorOpacity = 1;
          });
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          body: AnimatedOpacity(
            opacity: _indicatorOpacity,
            duration: Duration(milliseconds: 500),
            child: Stack(
              children: [
                Center(
                  child: SizedBox(
                      width: 125,
                      height: 125,
                      child: CircularProgressIndicator(
                        backgroundColor: AUColor.textFieldFill,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                          AUColor.primary,
                        ),
                        strokeWidth: 3,
                      )),
                ),
                Center(
                    child: CircleAvatar(
                  backgroundColor: AUColor.primary,
                  radius: 55,
                  child: Text(
                    'mono',
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: AUColor.onPrimary,
                        ),
                  ),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
