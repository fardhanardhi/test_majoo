import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_majoo/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_majoo/data/blocs/bottom_nav_cubit/bottom_nav_cubit.dart';
import 'package:test_majoo/util/au_color.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AuthCubit, AuthState>(
        builder: (context, state) {
          if (state is AuthAuthenticated) {
            String _name = state.user.name.characters
                .split(Characters(" "))
                .map((e) => e.first)
                .join()
                .padRight(5)
                .substring(0, 2)
                .trim();
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Material(
                    type: MaterialType.circle,
                    color: Colors.black,
                    elevation: 20,
                    child: CircleAvatar(
                      radius: 80,
                      backgroundColor: AUColor.primary,
                      child: Text(
                        _name,
                        style: Theme.of(context)
                            .textTheme
                            .headline1!
                            .copyWith(color: AUColor.onPrimary),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(state.user.name,
                      style: Theme.of(context).textTheme.headline5),
                  SizedBox(
                    height: 50,
                  ),
                  IconButton(
                    tooltip: "Logout",
                    iconSize: 50,
                    onPressed: () {
                      BlocProvider.of<BottomNavCubit>(context).navItemTapped(0);
                      BlocProvider.of<AuthCubit>(context).logout();
                    },
                    icon: Icon(Boxicons.bx_power_off),
                  ),
                ],
              ),
            );
          }
          return SizedBox();
        },
      ),
    );
  }
}
