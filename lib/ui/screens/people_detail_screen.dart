import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/data/blocs/people/delete_people/delete_people_cubit.dart';
import 'package:test_majoo/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_majoo/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_majoo/data/blocs/people/people_detail_cubit/people_detail_cubit.dart';
import 'package:test_majoo/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_majoo/data/models/people.dart';
import 'package:test_majoo/ui/screens/people_entry_screen.dart';
import 'package:test_majoo/ui/widgets/confirmation_dialog.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/au_ext.dart';
import 'package:test_majoo/util/transitions.dart';

class PeopleDetailScreen extends StatefulWidget {
  const PeopleDetailScreen({Key? key, required this.people}) : super(key: key);
  final People people;

  @override
  _PeopleDetailScreenState createState() => _PeopleDetailScreenState();
}

class _PeopleDetailScreenState extends State<PeopleDetailScreen> {
  late PeopleDetailCubit _peopleDetailCubit;
  late DeletePeopleCubit _deletePeopleCubit;
  late SetFavPeopleCubit _setFavPeopleCubit;

  @override
  void initState() {
    super.initState();
    _peopleDetailCubit = PeopleDetailCubit()..load(widget.people.id!);
    _deletePeopleCubit = DeletePeopleCubit();
    _setFavPeopleCubit = SetFavPeopleCubit();
  }

  void _handleDeletePeople() async {
    // LoadingDialog.show(context);
    await _deletePeopleCubit.delete(id: widget.people.id!);
    AUExt.popScreen(context);
  }

  @override
  void dispose() {
    _deletePeopleCubit.close();
    _setFavPeopleCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => _peopleDetailCubit,
        ),
        BlocProvider(
          create: (_) => _deletePeopleCubit,
        ),
        BlocProvider(
          create: (_) => _setFavPeopleCubit,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            bloc: _deletePeopleCubit,
            listener: (context, state) async {
              if (state is DeletePeopleFailure) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(state.message ?? "Delete failure"),
                    ),
                  );
                return;
              }
              if (state is DeletePeopleSuccess) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    const SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text("Delete people uccess"),
                    ),
                  );
                await BlocProvider.of<PeopleCubit>(context).reloadLocalData();
                await BlocProvider.of<FavPeopleCubit>(context)
                    .reloadFavLocalData();
                return;
              }
            },
          ),
          BlocListener(
            bloc: _setFavPeopleCubit,
            listener: (context, state) async {
              if (state is SetFavPeopleFailure) {
                if (state.message != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Failure add people to favorite'),
                    ),
                  );
                }
                return;
              }
              if (state is SetFavPeopleSuccess) {
                await _peopleDetailCubit.reload(widget.people.id!);
                await BlocProvider.of<PeopleCubit>(context).reloadLocalData();
                await BlocProvider.of<FavPeopleCubit>(context)
                    .reloadFavLocalData();
                return;
              }
            },
          ),
        ],
        child: Scaffold(
          body: BlocBuilder(
            bloc: _peopleDetailCubit,
            builder: (context, state) => ASharedAxisTransitionSwitcher(
              transitionType: SharedAxisTransitionType.vertical,
              fillColor: Colors.transparent,
              child: state is PeopleDetailLoading
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : state is PeopleDetailLoaded
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 50,
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        state.people!.name,
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                            fontSize: 25,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      Text(
                                        'Gender: ${state.people!.gender}',
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1,
                                      ),
                                    ],
                                  ),
                                  const Spacer(),
                                  IconButton(
                                    color: AUColor.primary.withOpacity(0.7),
                                    onPressed: () async =>
                                        await _setFavPeopleCubit.updateFav(
                                            people: state.people!),
                                    icon: ASharedAxisTransitionSwitcher(
                                      transitionType: state.people!.isFav
                                          ? SharedAxisTransitionType.vertical
                                          : SharedAxisTransitionType.scaled,
                                      child: state.people!.isFav
                                          ? const Icon(
                                              Boxicons.bxs_heart,
                                              key: ValueKey("fav"),
                                            )
                                          : const Icon(
                                              Boxicons.bx_heart,
                                              key: ValueKey("no"),
                                            ),
                                    ),
                                  ),

                                  // IconButton(
                                  //   onPressed: () async =>
                                  //       await _setFavPeopleCubit.updateFav(
                                  //           people: state.people!),
                                  //   icon: Icon(
                                  //     state.people!.favorite == 0
                                  //         ? Boxicons.bx_heart
                                  //         : Boxicons.bxs_heart,
                                  //     color: AUColor.error,
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 25),
                            Container(
                              color: AUColor.primary,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15)
                                              .copyWith(right: 15),
                                      decoration: const BoxDecoration(
                                        color: AUColor.surface,
                                        borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(30),
                                        ),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            child: Column(
                                              children: [
                                                Text(
                                                  state.people!.height,
                                                  style: const TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                const SizedBox(
                                                  height: 3,
                                                ),
                                                const Text(
                                                  'Height',
                                                  style: TextStyle(
                                                      color:
                                                          AUColor.accentText),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Expanded(
                                            child: Column(
                                              children: [
                                                Text(
                                                  state.people!.mass,
                                                  style: const TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                const SizedBox(
                                                  height: 3,
                                                ),
                                                const Text(
                                                  'Mass',
                                                  style: TextStyle(
                                                      color:
                                                          AUColor.accentText),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Expanded(
                                            child: Column(
                                              children: [
                                                Text(
                                                  state.people!.birthYear,
                                                  style: const TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                const SizedBox(
                                                  height: 3,
                                                ),
                                                const Text(
                                                  'Birth Year',
                                                  style: TextStyle(
                                                    color: AUColor.accentText,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  IconButton(
                                    onPressed: () async {
                                      bool refresh = await AUExt.pushScreen(
                                            context,
                                            PeopleEntryScreen(
                                              people: state.people,
                                            ),
                                          ) ??
                                          false;
                                      if (refresh) {
                                        _peopleDetailCubit
                                            .reload(widget.people.id!);
                                      }
                                    },
                                    icon: const Icon(
                                      Icons.edit,
                                      color: AUColor.onPrimary,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  IconButton(
                                    onPressed: () => ConfirmationDialog.show(
                                        context,
                                        onYes: _handleDeletePeople,
                                        desc:
                                            "Are you sure want to delete '${state.people!.name}'?"),
                                    icon: const Icon(
                                      Icons.delete,
                                      color: AUColor.onPrimary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 15),
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: Container(
                                padding: const EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(width: 1),
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        const Text("Hair color"),
                                        const Expanded(
                                          child: Divider(
                                            thickness: 1,
                                            indent: 5,
                                            endIndent: 5,
                                          ),
                                        ),
                                        Text(state.people!.hairColor),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        const Text("Eye color"),
                                        const Expanded(
                                          child: Divider(
                                            thickness: 1,
                                            indent: 5,
                                            endIndent: 5,
                                          ),
                                        ),
                                        Text(state.people!.eyeColor),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        const Text("Skin color"),
                                        const Expanded(
                                          child: Divider(
                                            thickness: 1,
                                            indent: 5,
                                            endIndent: 5,
                                          ),
                                        ),
                                        Text(state.people!.skinColor),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 15),
                          ],
                        )
                      : state is PeopleDetailLoadFailure
                          ? const Center(
                              child: Text("Load Data Failure"),
                            )
                          : const SizedBox.shrink(),
            ),
          ),
        ),
      ),
    );
  }
}

class _DetailItem extends StatelessWidget {
  const _DetailItem(
      {Key? key,
      required this.title,
      required this.icon,
      required this.desc,
      this.color})
      : super(key: key);
  final String title;
  final Icon icon;
  final String desc;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color ?? AUColor.primary,
      ),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AUColor.primary.withOpacity(0.25),
            ),
            padding: const EdgeInsets.all(7),
            child: icon,
          ),
          const SizedBox(
            width: 15,
          ),
          Expanded(
            child: Text(
              title,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Text(
            desc,
            style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }
}
