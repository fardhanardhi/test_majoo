import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_majoo/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_majoo/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_majoo/ui/screens/screens.dart';
import 'package:test_majoo/util/au_color.dart';
import 'package:test_majoo/util/au_ext.dart';
import 'package:test_majoo/util/transitions.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late PeopleCubit _peopleCubit;
  late SetFavPeopleCubit _setFavPeopleCubit;

  @override
  void initState() {
    super.initState();
    _peopleCubit = BlocProvider.of<PeopleCubit>(context)..load();
    _setFavPeopleCubit = SetFavPeopleCubit();
  }

  @override
  void dispose() {
    _setFavPeopleCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => _setFavPeopleCubit,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            bloc: _setFavPeopleCubit,
            listener: (context, state) async {
              if (state is SetFavPeopleFailure) {
                if (state.message != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Failure add people to favorite'),
                      backgroundColor: AUColor.primaryText,
                    ),
                  );
                }
                return;
              }
              if (state is SetFavPeopleSuccess) {
                await _peopleCubit.reloadLocalData(mustResetSearch: true);
                return;
              }
            },
          ),
        ],
        child: GestureDetector(
          onTap: () => AUExt.hideKeyboard(context),
          child: SafeArea(
            child: Scaffold(
              body: BlocBuilder(
                bloc: _peopleCubit,
                builder: (context, state) => ASharedAxisTransitionSwitcher(
                  transitionType: SharedAxisTransitionType.vertical,
                  fillColor: Colors.transparent,
                  child: state is PeopleLoading
                      ? const Center(
                          key: ValueKey('load'),
                          child: CircularProgressIndicator(),
                        )
                      : state is PeopleLoaded
                          ? Stack(
                              key: const ValueKey('success'),
                              children: [
                                ASharedAxisTransitionSwitcher(
                                  transitionType:
                                      SharedAxisTransitionType.vertical,
                                  child: state.people.isNotEmpty
                                      ? state.view == ViewPeople.list
                                          ? ListView.separated(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                0,
                                                75,
                                                0,
                                                120,
                                              ),
                                              itemBuilder: (context, index) {
                                                var _peopleItem =
                                                    state.people[index];
                                                String _name = _peopleItem
                                                    .name.characters
                                                    .split(Characters(" "))
                                                    .map((e) => e.first)
                                                    .join()
                                                    .padRight(5)
                                                    .substring(0, 2)
                                                    .trim();
                                                Widget widget = Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 5),
                                                    child:
                                                        AFadeThroughTransitionSwitcher(
                                                      child: OpenContainer(
                                                        openShape:
                                                            const RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius.vertical(
                                                                  bottom: Radius
                                                                      .circular(
                                                                          30)),
                                                        ),
                                                        closedElevation: 0,
                                                        closedShape:
                                                            const RoundedRectangleBorder(
                                                          borderRadius: BorderRadius
                                                              .horizontal(
                                                                  left: Radius
                                                                      .circular(
                                                                          50)),
                                                        ),
                                                        closedBuilder: (context,
                                                                openContainer) =>
                                                            ListTile(
                                                          key: ValueKey(
                                                              _peopleItem.id),
                                                          shape:
                                                              const RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .horizontal(
                                                              left: Radius
                                                                  .circular(50),
                                                            ),
                                                          ),
                                                          contentPadding:
                                                              const EdgeInsets
                                                                  .symmetric(
                                                            horizontal: 20,
                                                          ).copyWith(left: 15),
                                                          leading: CircleAvatar(
                                                            backgroundColor:
                                                                AUColor.primary,
                                                            foregroundColor:
                                                                AUColor
                                                                    .onPrimary,
                                                            child: Text(_name),
                                                          ),
                                                          trailing: IconButton(
                                                            color: AUColor
                                                                .primary
                                                                .withOpacity(
                                                                    0.7),
                                                            onPressed: () async =>
                                                                await _setFavPeopleCubit
                                                                    .updateFav(
                                                                        people:
                                                                            _peopleItem),
                                                            icon:
                                                                ASharedAxisTransitionSwitcher(
                                                              transitionType: _peopleItem
                                                                      .isFav
                                                                  ? SharedAxisTransitionType
                                                                      .vertical
                                                                  : SharedAxisTransitionType
                                                                      .scaled,
                                                              child: _peopleItem
                                                                      .isFav
                                                                  ? const Icon(
                                                                      Boxicons
                                                                          .bxs_heart,
                                                                      key: ValueKey(
                                                                          "fav"),
                                                                    )
                                                                  : const Icon(
                                                                      Boxicons
                                                                          .bx_heart,
                                                                      key: ValueKey(
                                                                          "no"),
                                                                    ),
                                                            ),
                                                          ),
                                                          onTap: () {
                                                            FocusManager
                                                                .instance
                                                                .primaryFocus
                                                                ?.unfocus();
                                                            openContainer();
                                                          },
                                                          title: Text(
                                                            state.people[index]
                                                                .name,
                                                          ),
                                                          subtitle: Text(state
                                                              .people[index]
                                                              .gender),
                                                        ),
                                                        openBuilder: (context,
                                                                tes) =>
                                                            PeopleDetailScreen(
                                                          people: state
                                                              .people[index],
                                                        ),
                                                      ),
                                                    ));

                                                if (index == 0) {
                                                  return Column(
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .fromLTRB(
                                                          5,
                                                          0,
                                                          5,
                                                          0,
                                                        ),
                                                        child: Row(
                                                          children: [
                                                            InkWell(
                                                              onTap: () async =>
                                                                  await _peopleCubit
                                                                      .sort(),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50),
                                                              child: Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        10,
                                                                    horizontal:
                                                                        15),
                                                                child: Row(
                                                                  children: [
                                                                    Text(
                                                                      "Name",
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .subtitle1,
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 5,
                                                                    ),
                                                                    Icon(
                                                                      state is PeopleLoaded && state.sort == SortPeople.desc
                                                                          ? Boxicons
                                                                              .bx_sort_z_a
                                                                          : Boxicons
                                                                              .bx_sort_a_z,
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            const Spacer(),
                                                            InkWell(
                                                              onTap: () {
                                                                AUExt.hideKeyboard(
                                                                    context);
                                                                AUExt
                                                                    .pushScreen(
                                                                  context,
                                                                  const PeopleEntryScreen(),
                                                                );
                                                              },
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50),
                                                              child: Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        10,
                                                                    horizontal:
                                                                        15),
                                                                child: Row(
                                                                  children: [
                                                                    Text(
                                                                      "Add",
                                                                      style: Theme.of(
                                                                              context)
                                                                          .textTheme
                                                                          .subtitle1,
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 5,
                                                                    ),
                                                                    const Icon(
                                                                        Boxicons
                                                                            .bx_plus),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 15,
                                                      ),
                                                      widget,
                                                    ],
                                                  );
                                                }

                                                return widget;
                                              },
                                              separatorBuilder:
                                                  (context, index) =>
                                                      const SizedBox(),
                                              itemCount: state.people.length)
                                          : GridView.builder(
                                              physics:
                                                  const BouncingScrollPhysics(),
                                              gridDelegate:
                                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                                childAspectRatio: 4 / 3,
                                                crossAxisCount: 2,
                                                mainAxisSpacing: 13,
                                                crossAxisSpacing: 13,
                                              ),
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                20,
                                                85,
                                                20,
                                                120,
                                              ),
                                              itemCount: state.people.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                          int index) =>
                                                      OpenContainer(
                                                openShape:
                                                    const RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.vertical(
                                                          bottom:
                                                              Radius.circular(
                                                                  30)),
                                                ),
                                                closedElevation: 0,
                                                closedShape:
                                                    RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  side: const BorderSide(
                                                    width: 2,
                                                  ),
                                                ),
                                                openBuilder: (context, tes) =>
                                                    PeopleDetailScreen(
                                                  people: state.people[index],
                                                ),
                                                closedBuilder:
                                                    (context, openContainer) =>
                                                        Card(
                                                  margin: EdgeInsets.zero,
                                                  elevation: 0,
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: Stack(
                                                      children: [
                                                        Positioned.fill(
                                                          child: InkWell(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                            onTap:
                                                                openContainer,
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                              child: Wrap(
                                                                children: [
                                                                  Center(
                                                                    child:
                                                                        Column(
                                                                      children: [
                                                                        Text(
                                                                          state
                                                                              .people[index]
                                                                              .name,
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .subtitle1
                                                                              ?.copyWith(fontWeight: FontWeight.w700),
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              10,
                                                                        ),
                                                                        Row(
                                                                          children: [
                                                                            Text(
                                                                              'Gender',
                                                                              style: Theme.of(context).textTheme.caption,
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                            Spacer(),
                                                                            Text(
                                                                              state.people[index].gender,
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              3,
                                                                        ),
                                                                        Row(
                                                                          children: [
                                                                            Text(
                                                                              "Height",
                                                                              style: Theme.of(context).textTheme.caption,
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                            Spacer(),
                                                                            Text(
                                                                              state.people[index].height + " cm",
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        Row(
                                                                          children: [
                                                                            Text(
                                                                              'Mass',
                                                                              style: Theme.of(context).textTheme.caption,
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                            Spacer(),
                                                                            Text(
                                                                              state.people[index].mass + ' kg',
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        Align(
                                                          alignment: Alignment
                                                              .bottomCenter,
                                                          child: IconButton(
                                                            color: AUColor
                                                                .primary
                                                                .withOpacity(
                                                                    0.7),
                                                            onPressed: () async =>
                                                                await _setFavPeopleCubit.updateFav(
                                                                    people: state
                                                                            .people[
                                                                        index]),
                                                            icon:
                                                                ASharedAxisTransitionSwitcher(
                                                              transitionType: state
                                                                      .people[
                                                                          index]
                                                                      .isFav
                                                                  ? SharedAxisTransitionType
                                                                      .vertical
                                                                  : SharedAxisTransitionType
                                                                      .scaled,
                                                              child: state
                                                                      .people[
                                                                          index]
                                                                      .isFav
                                                                  ? const Icon(
                                                                      Boxicons
                                                                          .bxs_heart,
                                                                      key: ValueKey(
                                                                          "fav"),
                                                                    )
                                                                  : const Icon(
                                                                      Boxicons
                                                                          .bx_heart,
                                                                      key: ValueKey(
                                                                          "no"),
                                                                    ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                      : const Center(
                                          key: ValueKey('empty'),
                                          child: Text("Empty Data"),
                                        ),
                                ),
                              ],
                            )
                          : const Center(
                              key: ValueKey('fail'),
                              child: Text("Load Data Failure"),
                            ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
